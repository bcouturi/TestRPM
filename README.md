Test setup to check RPM dependencies
====================================


Creating the packages
---------------------

Juts run

```
$ createall.sh
```

You should have the RPMs in */tmp/buildarea/rpmbuild/RPMS/noarch/*


Testing the installation with YUM
---------------------------------

Run createrepo in /tmp/buildarea/rpmbuild/RPMS/noarch/ and make sure it is visible by a webserver.
Configure you client to see that new repo and test...


