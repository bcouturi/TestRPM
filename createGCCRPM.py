#!/usr/bin/env python
"""
Command to create a test RPM
"""

import logging
import os
import subprocess
import sys

__RCSID__ = "$Id"

log = logging.getLogger()
log.addHandler(logging.StreamHandler())
log.setLevel(logging.INFO)

local_directory = os.path.dirname(__file__).strip()
os.environ['PATH'] =  os.pathsep.join([os.environ['PATH'], local_directory])

# Util method to prepare the command
################################################################################
def addDefine(param, val):
    """ Util method to add a define statement"""
    if val == None or val == '':
        val = '""'
    return " --define '%s %s' " % (param, val)


# Checking arguments
################################################################################
def usage():
    """ Display help"""
    print >> sys.stderr, "Usage: %s name version release" % sys.argv[0]
    sys.exit(2)

from optparse import OptionParser
parser = OptionParser()
parser.add_option("--requires", dest="requires", action="store",
                  help="Name the file with the list of requires",
                  metavar="REQUIRES", default = None)
parser.add_option("--name", dest="name", action="store",
                  help="name of the RPM",
                  metavar="RELEASE", default = "gcc")
parser.add_option("--release", dest="release", action="store",
                  help="release number",
                  metavar="RELEASE", default = "1")
parser.add_option("--buildarea", dest="buildarea", action="store",
                  help="Location where the RPM is build",
                  default = "/tmp/buildarea")
(options, args) = parser.parse_args()

if  len(args) < 1:
    print >> sys.stderr, "Missing arguments!"
    usage()

# We allow the hat and package name to be passed separated by
# spaces, this makes scripts invoking simpler in some cases
version = args[0]
name = options.name
release = options.release
maj_name = name + version.split(".")[0]

log.info("Creating <%s> <%s> <%s>" % (name, version, release))
buildarea = options.buildarea

try:
    os.makedirs(os.path.join(buildarea, "rpmbuild", "BUILD"))
    os.makedirs(os.path.join(buildarea, "rpmbuild", "RPMS"))
    os.makedirs(os.path.join(buildarea, "tmpbuild"))
    os.makedirs(os.path.join(buildarea, "tmp"))
except:
    pass
    # They may already be there...

cmd = "rpmbuild -v"
cmd += addDefine("build_area", buildarea)
cmd += addDefine("name", name)
cmd += addDefine("version", version)
cmd += addDefine("release", release)
cmd += addDefine("maj_name", maj_name)
cmd += " -bb " + os.path.join(local_directory, "gcc.spec")

log.info("Running: %s" % cmd)
rc = subprocess.call(cmd, shell=True)



