#!/bin/sh

for maj in `seq 6 7`; do 
    for min in `seq 1 3`; do
	python createGCCRPM.py ${maj}.${min}.0
    done
done

python createmypackage.py 1.0.0 gcc6
python createmypackage.py 2.0.0 gcc7
python createmypackage.py 2.1.0 gcc7
